import React from 'react';

var ProfileMainContent = class ProfileMainContent extends React.Component {

    render(){
        return(
            <section className="main-content">
                <div className="container">
                    <div className="row main-pad-row">
                        <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-pad-col">
                            <section className="profile-basic-info">
                                <div className="basic-info-content">
                                    <h3>Basic information</h3>
                                    <ul>
                                        <li>
                                            <span>Cricket</span>
                                            <span>Kings eleven Panjab, Kerala stat team </span>

                                        </li>
                                        <li><span>Football</span>
                                        <span>Mohan bagan FC ,  Karnataka football association</span>

                                    </li>
                                    <li><a className="add-link" href="#"><i className="ion-android-add" aria-hidden="true"></i><span>Add another  Sports</span></a> </li>
                                    <li><span>Age</span>
                                    <span>Apr 24, 1990 (22 years)</span>

                                </li>
                                <li><span>Location of residence </span>
                                <span>Bombay (now Mumbai), Maharashtra, India</span>

                            </li>
                        </ul>
                    </div>
                    <div id="address" className="basic-info-address">
                        <div className="info-address-inner">
                            <h3>view your connection</h3>
                            <div className="row address-content">
                                <div className="col-lg-6">
                                    <ul>
                                        <li className="clearfix">
                                            <div className="col-lg-3">Email</div>
                                            <div className="cont-text-pane"><a className="field-text"> ArunMahan@mail.com</a></div>
                                            <div className="cont-icon-pane">
                                                <a href="#" className="edit-icon ion-edit"></a>
                                            </div>
                                        </li>
                                        <li className="clearfix">
                                            <div className="col-lg-3">IM</div>
                                            <div className="cont-text-pane"></div>
                                            <div className="cont-icon-pane">
                                                <a href="#" className="edit-icon ion-edit"></a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-lg-6">
                                    <ul>
                                        <li className="clearfix">
                                            <div className="col-lg-3">Phone</div>
                                            <div className="cont-text-pane"><a className="field-text"> 959954656652 (mobile)</a></div>
                                            <div className="cont-icon-pane">
                                                <a href="#" className="edit-icon ion-edit"></a>
                                            </div>
                                        </li>
                                        <li className="clearfix">
                                            <div className="col-lg-3">Address</div>
                                            <div className="cont-text-pane"><a className="field-text address-pane"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse</a></div>
                                            <div className="cont-icon-pane">
                                                <a href="#" className="edit-icon ion-edit"></a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="basic-info-foot clearfix">
                        <div className="pull-left"></div>
                        <div className="pull-right">
                            <a id="contact-btn" className="info-address"><i className="ion-android-list"></i>Contact</a>
                        </div>
                    </div>
                </section>
                <section className="tab-container">
                    <ul className="nav nav-tabs">
                        <li className="active">
                            <a data-toggle="tab" href="#spot">
                                <span className="tab-icon sports"></span>
                                <span className="tab-text">Sports</span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#result">
                                <span className="tab-icon result"></span>
                                <span className="tab-text">Sports</span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#gallery">
                                <span className="tab-icon gallery"></span>
                                <span className="tab-text">Sports</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <section className="panel panel-default">
                    <div className="panel-body">
                        <div className="tab-content">
                            <div id="spot" className="tab-pane fade in active">
                                <div className="spot-data-head clearfix row">
                                    <div className="col-lg-2 spot-item-logo">
                                        <img src="img/spot-logo.png" className="img-responsive" alt="image" />
                                    </div>
                                    <div className="col-lg-10 spot-spacification">
                                        <h4>Football </h4>
                                        <div className="spot-position">
                                            <span>Position</span>
                                            <span>Midfielder</span>
                                        </div>
                                        <ul className="list-inline">
                                            <li>
                                                <span className="spot-value">20</span>
                                                <span className="spot-tag">Goals</span>
                                            </li>
                                            <li><span className="spot-value">20</span>
                                            <span className="spot-tag">Appearances</span></li>
                                            <li><span className="spot-value">20</span>
                                            <span className="spot-tag">Win</span></li>
                                            <li><span className="spot-value">14</span>
                                            <span className="spot-tag">Draws</span></li>
                                            <li><span className="spot-value">6</span>
                                            <span className="spot-tag">Losses</span></li>
                                        </ul>
                                    </div>

                                </div>
                                <div className="content-inner-tab">
                                    <ul className="nav nav-tabs">
                                        <li className="active"><a data-toggle="tab" href="#career">Career</a></li>
                                        <li><a data-toggle="tab" href="#award">Award and achievements</a></li>
                                        <li><a data-toggle="tab" href="#result">Results</a></li>
                                    </ul>
                                    <div className="tab-content">
                                        <div id="career" className="tab-pane fade in active">
                                            <div className="creer-result">
                                                <ul>
                                                    <li className="tbl-cupname">
                                                        <div className="label-data"><span className="text">FRIENDLY LEAGUE</span></div>
                                                    </li>
                                                    <li className="tbl-edition-wins">

                                                        <ul className="editionwins-list">
                                                            <li>
                                                                <div className="cup-image">
                                                                    <img src="http://img.fifa.com/images/tournaments/17/trophy_off.png"/>
                                                                    </div>
                                                                    <span className="text">2014</span>
                                                                    </li>
                                                                <li>
                                                                    <div className="cup-image"><img src="http://img.fifa.com/images/tournaments/17/trophy_off.png"/></div><span className="text">2010</span></li>
                                                                    <li>
                                                                        <div className="cup-image"><img src="http://img.fifa.com/images/tournaments/17/trophy_off.png"/></div><span className="text">2006</span></li>
                                                                    </ul>

                                                                </li>
                                                                <li className="tbl-goalfor">
                                                                    <div className="label-name">Goals scored </div>
                                                                    <div className="label-data"><span className="text">3</span></div>
                                                                </li>
                                                                <li className="tbl-cards">
                                                                    <ul>
                                                                        <li className="tbl-y">
                                                                            <div className="label-name">Yellow cards </div>
                                                                            <div className="label-data"><span className="text">2</span></div>
                                                                        </li>
                                                                        <li className="tbl-rc">
                                                                            <div className="label-name">Red Cards </div>
                                                                            <div className="label-data"><span className="text">0</span></div>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li className="tbl-match-played">
                                                                    <ul>
                                                                        <li className="tbl-matches-num-wdl">
                                                                            <div className="label-name">Matches </div>
                                                                            <div className="label-data"><span className="text">13</span></div>
                                                                        </li>
                                                                        <li className="tbl-win" style={{width:'38.46153846153847%'}}></li>
                                                                        <li className="tbl-draw" style={{width:'30.76923076923077%'}}></li>
                                                                        <li className="tbl-lost" style={{width:'30.76923076923077%'}}></li>
                                                                        <li className="tbl-win data">
                                                                            <div className="label-data"><span className="text">5 W</span></div>
                                                                        </li>
                                                                        <li className="tbl-draw data">
                                                                            <div className="label-data"><span className="text">4 D</span></div>
                                                                        </li>
                                                                        <li className="tbl-lost data">
                                                                            <div className="label-data"><span className="text">4 L</span></div>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li className="tbl-expand">
                                                                    <a href="#" className="ion-chevron-down"></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div className="career-details">
                                                            sdsa
                                                        </div>
                                                    </div>
                                                    <div id="award" className="tab-pane fade">
                                                        <h3>Menu 1</h3>
                                                        <p>Some content in menu 1.</p>
                                                    </div>
                                                    <div id="result" className="tab-pane fade">
                                                        <h3>Menu 2</h3>
                                                        <p>Some content in menu 2.</p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div id="result" className="tab-pane fade">
                                            <h3>Menu 1</h3>
                                            <p>Some content in menu 1.</p>
                                        </div>
                                        <div id="gallery" className="tab-pane fade">
                                            <h3>Menu 2</h3>
                                            <p>Some content in menu 2.</p>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 main-pad-col">
                            <section className="panel panel-default">
                                <header className="panel-heading">
                                    <h3 className="panel-title">Major Sponsor</h3>

                                </header>
                                <div className="panel-body">
                                    <img src="img/sponser.jpg" className="img-responsive" alt="image" />
                                </div>
                            </section>
                            <section className="panel panel-default">
                                <header className="panel-heading">
                                    <h3 className="panel-title">Previous Events</h3>

                                </header>
                                <div className="panel-body">
                                    <div className="previous-event">
                                        <div className="event-head clearfix">
                                            <h3>INDIAN PREMIER LEAGUE</h3><span className="event-date">Tue Sep 20</span></div>
                                            <ul className="vertical-list">
                                                <li className="border-btm clearfix"> <span><img src="img/team-logo.jpg" className="img-responsive"/></span><span> Kings eleven Panjab</span><span>154</span></li>
                                                <li className="border-btm clearfix"><span><img src="img/team-logo.jpg" className="img-responsive"/></span><span> Mumbai Indians</span><span>154</span></li>
                                            </ul>
                                            <div className="event-head clearfix">
                                                <h3>INDIAN PREMIER LEAGUE</h3><span className="event-date">Tue Sep 20</span></div>
                                                <ul className="vertical-list">
                                                    <li className="border-btm clearfix"> <span><img src="img/team-logo.jpg" className="img-responsive"/></span><span> Kings eleven Panjab</span><span>154</span></li>
                                                    <li className="border-btm clearfix"><span><img src="img/team-logo.jpg" className="img-responsive"/></span><span> Mumbai Indians</span><span>154</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>
        )

    }
}

module.exports = ProfileMainContent;
