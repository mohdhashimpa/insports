import React from 'react'

var BgImage = class BgImage extends React.Component {
    render(){
        return(
            <div className="profile-bg">
                <img src="img/header-bg.jpg" className="img-responsive" alt="image" />
            </div>
        )
    }
}
module.exports = BgImage;
