import React from 'react'

var ProfileContent = class ProfileContent extends React.Component {
    render(){
        return(
            <span>
                <div className="profile-block text-center">
                    <span className="profile-avatar">
                        <img src="img/profile-image.jpg" className="img-responsive" alt="image" />
                    </span>
                    <h3>Popular Name</h3>
                    <p>Real Name</p>
                    <p>Cochin, Kerala, India.</p>
                </div>
                <div className="profile-followers">
                    <span>251</span>
                    <span>Followers</span>
                </div>
                <div className="profile-following">
                    <span>10</span>
                    <span>Following</span>
                </div>
            </span>
        )
    }
}
module.exports = ProfileContent;
