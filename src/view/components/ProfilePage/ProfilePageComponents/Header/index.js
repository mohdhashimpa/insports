import React from 'react'

import BgImage from './BgImage'
import HeaderProfileContent from './HeaderProfileContent'

var ProfileHeader = class ProfileHeader extends React.Component {
    render(){
        return(
            <section className="profile-header">
                <BgImage />
                <div className="profile-cont-block">
                    <div className="container">
                        <div className="profile-content">
                            <a href="/" className="background-image"><i className="ion-ios-camera"></i>Edit Your Background</a>
                            <HeaderProfileContent />
                            <div className="follow-button"><a href="#">Follow</a></div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

module.exports = ProfileHeader
