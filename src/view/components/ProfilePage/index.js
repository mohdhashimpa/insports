import React from 'react';

import ProfileHeader from 'components/ProfilePage/ProfilePageComponents/Header'
import ProfileMainContent from './ProfilePageComponents/ProfileMainContent'


var ProfileComponent = class ProfileComponent extends React.Component {
    render() {
        return (
            <div>
                <ProfileHeader />
                <ProfileMainContent />
            </div>
        );
    }
}

module.exports = ProfileComponent;
