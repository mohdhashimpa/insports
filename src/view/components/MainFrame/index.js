import React from 'react';
import { hashHistory,Link } from 'react-router'
// import 'public/css/bootstrap.css'
import 'public/css/header.css'
import 'public/css/layout.css'
import 'public/css/top-nav.css'
import _ from "lodash"

import SearchAction from 'components/MainFrame/action/onSearch';

import SearchResultStore from 'components/MainFrame/store/SearchResultStore';

import ProfileComponent from 'components/ProfilePage';


var MainFrame = React.createClass({
	search : function(keyword){
		SearchAction.search(keyword);
	},
	render: function(){
		return (
			<div>
				<header id="header-global">
					<div className="container">
						<div className="header-left">
							<div className="pull-left">
								<h1 className="logo" data-sr="wait .9s, enter left, hustle 40px">
									<a href="/" id="logoa" className="jx-pg-link">
										<img src="img/logo.png" className="img-responsive" alt="Insports Logo" id="logo-b"  />
									</a>
								</h1>
							</div>
							<div className="pull-left main-search">
								<ul className="clearfix">
									<li className="dropdown">
										<span className="advance-selection dropdown-toggle" data-toggle="dropdown">
											<i className="ion-ios-arrow-down"></i>
										</span>
										<ul className="dropdown-menu">
											<li><a href="#">HTML</a></li>
											<li><a href="#">CSS</a></li>
											<li><a href="#">JavaScript</a></li>
										</ul>
									</li>
									<li className="search-input">
										<input name="keywords" id="main-search-box" type="text" value="" dir="ltr" placeholder="Search for people, sports, Coches, and more..." />
											<button type="button">
												<i className="ion-ios-search-strong"></i>
											</button>
										</li>
									</ul>
								</div>
							</div>
							<div className="header-right">
								<div className="pull-right">
									<button className="navbar-toggle collapsed no-mg-rgt" data-toggle="collapse" data-target="#bs-navbar">
										<span className="icon-bar nav-menu-icon"></span>
										<span className="icon-bar nav-menu-icon"></span>
										<span className="icon-bar nav-menu-icon"></span>
									</button>
								</div>
								<div className="pull-right drop-menu-container">
									<nav id="bs-navbar" className="collapse navbar-collapse no-bdr-top" data-sr="wait .9s, enter right, hustle 40px">
										<ul className="nav navbar-nav">
											<li><a href="#"><i className="ion-chatboxes"></i><span className="label">1</span></a></li>
											<li><a href="#"><i className="ion-flag"></i><span className="label">2</span></a></li>
											<li><a href="#"><i className="ion-person-add"></i><span className="label">1</span></a></li>
											<li className="dropdown">
												<a href="#" data-toggle="dropdown">
													<span className="profile-image"></span>
													<span className="profile-name">Consectetur</span>
													<span className="ion-android-arrow-dropdown drop-arrow"></span>

												</a>
												<ul className="dropdown-menu">
													<li><a href="#">HTML</a></li>
													<li><a href="#">CSS</a></li>
													<li><a href="#">JavaScript</a></li>
												</ul>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</header>
					<section id="wrapper">
						<section className="top-nav clearfix">
							<div className="container">
								<div className="pull-left">
									<ul className="nav navbar-nav">
										<li>
											<a href="/">Home</a>
										</li>
										<li>
											<a href="/">Profile</a>
										</li>
										<li>
											<a href="/">Network</a>
										</li>

									</ul>
								</div>
								<div className="pull-right">
									<a className="membership" href="#">Try Premium for free</a>
								</div>
							</div>
						</section>
					</section>
					<section>
						<ProfileComponent />
					</section>
				</div>
			);
		}
	});

	module.exports = MainFrame;
